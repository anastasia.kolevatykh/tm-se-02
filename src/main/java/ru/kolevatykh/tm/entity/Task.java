package ru.kolevatykh.tm.entity;

public class Task {

    private long projectId;
    private long id = System.nanoTime();
    private String taskName;
    private String taskContent;

    public Task() {
    }

    public Task(long projectId, String taskName, String taskContent) {
        this.projectId = projectId;
        this.taskName = taskName;
        this.taskContent = taskContent;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskContent(String taskContent) {
        this.taskContent = taskContent;
    }

    public String getTaskContent() {
        return taskContent;
    }

    public long getProjectId() {
        return projectId;
    }

    @Override
    public String toString() {
        return "Task{ " +
                "project id: " + projectId +
                ", task name: '" + taskName + '\'' +
                ", task content:'" + taskContent + '\'' +
                '}';
    }
}
