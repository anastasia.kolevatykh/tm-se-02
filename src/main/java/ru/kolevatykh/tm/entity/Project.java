package ru.kolevatykh.tm.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Project {
    private String projectName;
    private long id = System.nanoTime();

    public Project() {

    }

    public Project(String projectName) {
        this.projectName = projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public long getProjectId() {
        return id;
    }

    @Override
    public String toString() {
        return projectName;
    }
}
