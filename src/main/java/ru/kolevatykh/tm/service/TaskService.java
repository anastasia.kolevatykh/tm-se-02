package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskService {
    private List<Task> tasks = new ArrayList<>();

    public void create(Task task) {
        tasks.add( task );
    }

    public void readById(int id) {

    }

    public void readAll() {
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
    }

    public void clearAll() {
        tasks = new ArrayList<>();
    }

    public void removeByName(long projectId, String taskName) {
        Iterator<Task> it = tasks.iterator();
        while (it.hasNext()) {
            Task task = it.next();
            if (taskName.equals(task.getTaskName())
                    && (projectId == task.getProjectId())) {
                it.remove();
            }
        }

    }

    public void removeByProjectId(long projectId) {
        Iterator<Task> it = tasks.iterator();
        while (it.hasNext()) {
            Task task = it.next();
            if (projectId == task.getProjectId()) {
                it.remove();
            }
        }
    }
}
